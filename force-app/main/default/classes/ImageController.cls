public with sharing class ImageController {

    @AuraEnabled(Cacheable=true)
    public static Image__c[] getAllImages() {
        return [SELECT Id__c, Title__c, AlbumId__c, ThumbnailUrl__c, Url__c
            FROM Image__c ORDER BY Id__c LIMIT 50];
    }

    @AuraEnabled(Cacheable=true)
    public static Image__c[] searchImages(String searchTerm) {
        searchTerm = searchTerm.trim();
        if (searchTerm == '') {

            return getAllImages();
        }
        searchTerm = '%'+ searchTerm +'%';
        
        return [SELECT Id__c, Title__c, AlbumId__c, ThumbnailUrl__c, Url__c
            FROM Image__c WHERE Title__c LIKE :searchTerm ORDER BY Id__c LIMIT 50];
    }  

    @AuraEnabled(Cacheable=true)
    public static void addImages() {
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://jsonplaceholder.typicode.com/photos');
        request.setHeader('Content-type', 'application/json');
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        
        Object[] results;
        system.debug('response'+ response.getStatusCode());
        system.debug('response'+ response.getBody());
        
        results = (Object[]) JSON.deserializeUntyped(response.getBody());

        // for the moment being everything is working fine, we can use 'results' as return and we will see all records in the 
        // imageList component, but when we want to save these records on salesforce, below code doesn't work 
        
        List<Image__c> newallImages = new List<Image__c>();
        List<NewImage> allImages = (List<NewImage>)JSON.deserialize(response.getBody(),List<NewImage>.class);
        for( NewImage image: allImages) {
        Image__c newImage = new Image__c();
        newImage.AlbumId__c = image.albumId;
        newImage.Id__c= image.id;
        newImage.ThumbnailUrl__c = image.thumbnailUrl;
        newImage.Url__c = image.url;
        newImage.Title__c = image.title;

        newallImages.add(newImage);    
        }
        Database.insert(newallImages);
    }  

    public class NewImage{

    Double albumId;
    Double id;
    String thumbnailUrl;
    String url;
    String title;
    }
}



