/* eslint-disable no-console */
import { LightningElement, track, wire, api } from 'lwc';
import searchImages from '@salesforce/apex/ImageController.searchImages';
import addImages from '@salesforce/apex/ImageController.addImages';
import {deleteRecord} from 'lightning/uiRecordApi';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';

export default class ImageList extends LightningElement {
	@track searchTerm = '';
	@track clickedButtonLabel;
	@wire(searchImages, {searchTerm: '$searchTerm'})
	images;

	handleSearchTermChange(event) {
		
		window.clearTimeout(this.delayTimeout);
		const searchTerm = event.target.value;
		// eslint-disable-next-line @lwc/lwc/no-async-operation 	 
		this.delayTimeout = setTimeout(() => {
			this.searchTerm = searchTerm;
			
		}, 300, );
	}

	get hasResults() {
		return (this.images.data.length > 0);
	}

	@api recordId;
    deleteImage__c(event) {

		this.addRec();
		const recordId = event.target.dataset.recordid;
        console.log('delete account' + recordId);
		
		deleteRecord(recordId)
            .then(() => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Record Is  Deleted',
                        variant: 'success',
                    }),
                );
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error While Deleting record',
                        message: error.message,
                        variant: 'error',
                    }),
                );
			});
	}
}